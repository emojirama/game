const emojirama = {
  menu: {
    exitToHome: "Go to the home screen?"
  },
  cancel: "Cancel",
  exit: "Exit"
};

export default emojirama;
