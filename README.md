# emojirama

To start the project locally, create a `.env` file in the root of the project by copying `.env.template`. Fill in or change any values that you need to, such as OAuth2 keys and secrets, or a local IP API access while testing on mobile devices.

```sh
docker-compose up
```

This project is on GitLab: [https://gitlab.com/emojirama/game](https://gitlab.com/emojirama/game).

## Deployment to DigitalOcean using Docker Swarm

- [ ] Create `stack.yml` file
- [ ] Add multistage build for NGINX service using quasar build files
- [ ] Create `.gitlab-ci.yml` for building to gitlab.com registry and deploying to docker swarm
- [ ] Document the environment variables needed and add to README.md and GitLab CI/CD variables for use in pipeline
- [ ] Adjust NGINX config file for asgiserver
- [ ] Remove CloudFormation code
- [ ] Create DigitalOcean Droplet to use for site
- [ ] Setup Droplet based on other article
- [ ] Update dependencies
- [ ] Remove unused dependencies
- [ ] Run locally
- [ ] Add personal access token to GitLab for deploying images


## When code is tagged...

1. Integration tests for python and js (test stage)
1. Build quasar app (build)
1. Build nginx image (release)
1. Build backend image
1.

# Environment Variables for backend container and frontend container