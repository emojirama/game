from .base import *  # noqa

# Email

EMAIL_USE_TLS = True
EMAIL_HOST_USER = os.environ.get(
    "DJANGO_EMAIL_HOST_USER", "user@gmail.com"
)  # noqa
EMAIL_HOST_PASSWORD = os.environ.get(
    "DJANGO_EMAIL_HOST_PASSWORD", "emailpassword"
)  # noqa


# Logging

log_level = "INFO"

LOGGING = {
    "version": 1,
    "disable_existing_loggers": False,
    "handlers": {"console": {"class": "logging.StreamHandler",},},
    "loggers": {
        "django": {
            "handlers": ["console"],
            "level": os.getenv("DJANGO_LOG_LEVEL", "INFO"),  # noqa
        },
    },
}
