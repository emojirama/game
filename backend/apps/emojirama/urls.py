from django.urls import path

from . import views

urlpatterns = [
    path(
        "emojirama/",
        views.EmojiramaViewSet.as_view(
            {"get": "list_emojiramas", "post": "new_emojirama"}
        ),
        name="emojirama",
    ),
    path(
        "emojirama/<str:uuid>/",
        views.EmojiramaViewSet.as_view(
            {"get": "get", "delete": "delete", "post": "save"}
        ),
        name="emojirama-record",
    ),
]
