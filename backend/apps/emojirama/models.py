from django.conf import settings
from django.db.models import JSONField
from django.db import models
from django_extensions.db.fields import ShortUUIDField

import shortuuid

from apps.core.models import BaseModel


def get_uuid():
    return shortuuid.uuid()


class Emojirama(BaseModel):
    board = JSONField()
    uuid = ShortUUIDField(default=get_uuid)
    owner = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        null=True,
        blank=True,
        related_name="owner",
        on_delete=models.CASCADE,
    )

    def scene_count(self):
        return len(self.board["scenes"])

    def __str__(self):
        return f"Emojirama {self.id}"

    class Meta:
        ordering = ("-created_on",)
