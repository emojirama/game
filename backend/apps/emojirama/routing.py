from django.urls import path

from . import consumers

websocket_urlpatterns = [
    path("ws/emojirama/<str:emojirama_uuid>/", consumers.CoreConsumer,),
    # url(r'^ws/chat/(?P<room_name>[^/]+)/$', consumers.CoreConsumer),
]
