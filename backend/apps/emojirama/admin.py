from django.contrib import admin
from django.utils.html import format_html

from .models import Emojirama


class EmojiramaAdmin(admin.ModelAdmin):
    list_display = (
        'id',
        '__str__',
        'uuid',
        'link',
        'scene_count',
        'owner',
        'created_by',
        'created_on',
    )

    def link(self, obj=None):
        return format_html(
            f'<a target="_blank" href="/emojirama/{obj.uuid}">Go</a>'
        )

    link.allow_tags = True


admin.site.register(Emojirama, EmojiramaAdmin)
