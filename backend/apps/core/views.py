from django.http import JsonResponse


def health_check(request):
    response = JsonResponse({"message": "OK"})
    return response
